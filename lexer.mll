{
  open Parser
  exception Eof
}

let aeiou = ['a' 'e' 'i' 'o' 'u']
let aeou = ['a' 'e' 'o' 'u']
let aei = ['a' 'e' 'i']
let u_AEIOU = ['A' 'E' 'I' 'O' 'U']
let klmnps = ['k' 'l' 'm' 'n' 'p' 's']
let klps = ['k' 'l' 'p' 's']
let u_KLMNPS = ['K' 'L' 'M' 'N' 'P' 'S']
let jt = ['j' 't']
let u_JT = ['J' 'T']
let n = ['n']
let w = ['w']
let u_W = ['W']

rule token = parse
  [' ' '\t']	{ token lexbuf }
| ['\n' '\r'] { Lexing.new_line lexbuf ; token lexbuf }
| "#" [^ '\n']* '\n'		{ Lexing.new_line lexbuf ; token lexbuf } (* skip #'d lines *)
| "nasin" { NASIN }
| "pali" { PALI }
| "ijo" { IJO }
| "nanpa" { NANPA }
| "nimi" { NIMI }
| "pona" { PONA }
| "pini" { PINI }
| "jo" { JO }
| "sama" { SAMA }
| "pi" { PI }
| "ni" { NI }
| "li" { LI }
| "la" { LA }
| "e" { E }
| "lon" { LON }
| "ala" { ALA }
| "wan" { WAN }
| "tu" { TU }
| "luka" { LUKA }
| "mute" { MUTE }
| "ali" { ALI }
| "ale" { ALI }
| "monsi" { MONSI }
| ('-'?)['0'-'9']+ as lxm { INT(int_of_string lxm) }
| (u_KLMNPS aeiou|u_AEIOU|u_JT aeou|u_W aei)(n klps aeiou|klmnps aeiou|n? jt aeou|n? w aei)*n? as lxm { ID(lxm) }
| ['A'-'Z''a'-'z''_']+ as lxm { Printf.printf "PAKALA: nimi \"%s\" li ike tawa toki pona.\n" lxm; failwith "Bad input" }
| eof { EOF }
| '"' { let buffer = Buffer.create 10 in
        STR(stringl buffer lexbuf)
      }
  and  stringl buffer = parse
  | '"' { Buffer.contents buffer }
  | "\\t" { Buffer.add_char buffer '\t'; stringl buffer lexbuf }
  | "\\n" { Buffer.add_char buffer '\n'; stringl buffer lexbuf }
  | '\\' '"' { Buffer.add_char buffer '"'; stringl buffer lexbuf }
  | '\\' '\\' { Buffer.add_char buffer '\\'; stringl buffer lexbuf }
  | eof { raise End_of_file }
  | _ as char { Buffer.add_char buffer char; stringl buffer lexbuf }
| _ as lxm { Printf.printf "Illegal character %c\n" lxm; failwith "Bad input" }
