%{
  open Ast ;;
  open Lexing ;;

  exception ParseErr of string

  let error msg start finish  =
    Printf.printf "PAKALA (linja %d: nimi %d..%d): %s" start.pos_lnum
        (start.pos_cnum -start.pos_bol) (finish.pos_cnum - finish.pos_bol) (msg ^ "\n")

  let parse_err msg nterm = error msg (rhs_start_pos nterm) (rhs_end_pos nterm)

  let global_binds = Hashtbl.create 3
  let local_binds = Hashtbl.create 3

  let is_bound id = (Hashtbl.mem global_binds id) || (Hashtbl.mem local_binds id)
%}

%token <int> INT
%token <string> STR
%token <string> ID
%token NASIN PALI NI SELO PINI TOKI LINJA WEKA AWEN OPEN JO SAMA LON
%token LI LA PI E
%token NANPA NIMI PONA IJO
%token ALA WAN TU LUKA MUTE ALI MONSI
%token EOF
%token ERR
%start main
%type <Ast.prog> main
%type <Ast.value list> param_list
%%

main
  : sents EOF { $1 }
  ;

sents
  : { [] }
  | sent sents { $1::$2 }
  ;

sent
  : fn { $1 }
  | glob { $1 }
  | print { $1 }
  ;

fn
  : NASIN ID mono_args LI expr { EFun($2, $3, ERet($5)) }
  | NASIN ID mono_args LA body { EFun($2, $3, $5) }
  | NASIN ID LI JO E predicate param_list LA body { EFun($2, $6::$7, $9) }
  ;

glob
  : IJO ID LI expr { EGlob($2, ERet($4)) }
  | IJO ID LA body { EGlob($2, $4) }
  ;

print
  : body { EPrint($1) }
  ;

body
  : NI LI expr { ERet($3) }
  | IJO ID LI expr LA body { EBind($2, $4, $6) }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

expr
  : value { EVal($1) }
  | ID { EVal(Id(Any, $1)) }
  | PALI ID arg_list { ECall($2, $3) }
  | PALI ID mono_args { ECall($2, $3) }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

param_list
  : { [] }
  | E predicate param_list { $2::$3 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

arg_list
  : { [] }
  | E value arg_list { $2::$3 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

predicate
  : literal { $1 }
  | IJO ID { Id(Any, $2) }
  | NANPA ID { Id(Int, $2) }
  | NIMI ID { Id(Str, $2) }
  | PONA ID { Id(Bool, $2) }
  | MUTE ID { Id(List, $2) }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

value
  : ID { Id(Any, $1) }
  | literal { $1 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

literal
  : INT { Lit(LInt($1)) }
  | STR { Lit(LStr($1)) }
  | LON { Lit(LBool(true)) }
  | LON ALA { Lit(LBool(false)) }
  | MUTE ALA { Lit(LList([])) }
  | NIMI ALA { Lit(LStr("")) }
  | compound_num { Lit(LInt($1)) }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

mono_arg
  : ID { Id(Any, $1) }
  | INT { Lit(LInt($1)) }
  | STR { Lit(LStr($1)) }
  | num { Lit(LInt($1)) }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

mono_args
  : { [] }
  | mono_arg mono_args { $1::$2 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

num
  : ALI { 100 }
  | MUTE { 20 }
  | LUKA { 5 }
  | TU { 2 }
  | WAN { 1 }
  | ALA { 0 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

compound_num
  : compound_num MONSI { ($1) * (-1) }
  | alis { $1 }
  | ALA { 0 }
  | error { parse_err "nimi ni li ike" 1 ; raise (ParseErr "") }
  ;

alis
  : ALI { 100 }
  | ALI alis { 100 + $2 }
  | mutes { $1 }
  ;

mutes
  : MUTE { 20 }
  | MUTE mutes { 20 + $2 }
  | lukas { $1 }
  ;

lukas
  : LUKA { 5 }
  | LUKA lukas { 5 + $2 }
  | tus { $1 }
  ;

tus
  : TU { 2 }
  | TU tus { 2 + $2 }
  | wans { $1 }
  ;

wans
  : WAN { 1 }
  | WAN wans { 1 + $2 }
  ;
