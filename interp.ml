open Ast

let rec string_of_value v = match v with
  | LInt i -> string_of_int i
  | LStr s -> s
  | LBool b -> if b then "true" else "false"
  | LList l -> List.fold_left (fun acc v -> acc ^ (string_of_value v)) "" l

(* Returns list of all ids within a value list *)
let rec get_ids vl = match vl with
  | [] -> []
  | h::t -> match h with
    | Id (_, id) -> id::(get_ids t)
    | Lit _ -> (get_ids t)

let get_binding id (_, globals) locs : literal =
  if Hashtbl.mem locs id
    then Hashtbl.find locs id
  else if Hashtbl.mem globals id
    then Hashtbl.find globals id
  else failwith ("Unbound variable \"" ^ id ^ "\"!")

let rec get_bindings vl env locs : literal list =
  match vl with
  | [] -> []
  | h::t -> match h with
    | Lit l -> l::(get_bindings t env locs)
    | Id (_, id) -> (get_binding id env locs)::(get_bindings t env locs)

let core_fns = ["Tasu"; "Siku"; "Kake"; "Kipi"; "Motu"; "Neke"; "Konka"; "Mute"; "Konsu"; "Na"; "Anpa"; "Insasin"; "Len"; "Asuki"]
let is_core_fn name = List.fold_left (fun acc n -> n == name || acc) false core_fns

let add_core_fn fn_table name =
  Hashtbl.add fn_table (name, 0) [{name = name; args = []; body = ERet(EVal(Lit(LInt(-1))))}]

let rec list_match l1 l2 =
  (List.length l1) == (List.length l2) &&
  (List.fold_left2 (fun acc lit1 lit2 -> acc && (literal_match lit1 lit2)) true l1 l2)

and literal_match l1 l2 = match l1 with
  | LInt i1 -> (match l2 with LInt i2 -> i1 == i2 | _ -> false)
  | LStr s1 -> (match l2 with LStr s2 -> String.equal s1 s2 | _ -> false)
  | LBool b1 -> (match l2 with LBool b2 -> b1 == b2 | _ -> false)
  | LList ls1 -> (match l2 with LList ls2 -> (list_match ls1 ls2) | _ -> false)

let type_match (t: t) l = match t with
  | Any -> true
  | Int -> (match l with LInt _ -> true | _ -> false)
  | Str -> (match l with LStr _ -> true | _ -> false)
  | Bool -> (match l with LBool _ -> true | _ -> false)
  | List -> (match l with LList _ -> true | _ -> false)

let arg_match a v = match a with
  | Id (t, _) -> type_match t v
  | Lit l -> literal_match l v

let args_match al (ll: literal list) =
  List.fold_left2 (fun acc a l -> (arg_match a l) && acc) true al ll

let rec fn_match fns args = match fns with
  | [] -> None
  | fn::t -> if args_match fn.args args then Some(fn) else fn_match t args

let bind_arg_ids al vl = List.fold_left2
  (fun acc a v -> match a with
    | Id (_, id) -> (Hashtbl.add acc id v ; acc)
    | Lit _ -> acc)
  (Hashtbl.create (List.length vl)) al vl

let rec fn_dispatch name args (fn_table, globals) locs =
  (*let _ = Printf.printf "%s: %s\n" name (string_of_value (LList(args))) in*)
  (*let _ = flush_all () in*)
  match name with
  | "Tasu" -> LInt(List.fold_left
      (fun acc l -> match l with LInt i -> i + acc | _ -> failwith "Bad args on Tasu" )
      0 args)
  | "Siku" -> LInt(List.fold_right
      (fun l acc -> match l with LInt i -> i - acc | _ -> failwith "Bad args on Siku" )
      args 0)
  | "Kake" -> LInt(List.fold_left
      (fun acc l -> match l with LInt i -> i * acc | _ -> failwith "Bad args on Kake" )
      1 args)
  | "Kipi" -> LInt(List.fold_right
      (fun l acc -> match l with LInt i -> i / acc | _ -> failwith "Bad args on Kipi" )
      args 1)
  | "Motu" -> (match List.nth args 0 with
    | LInt i1 ->
    (match List.nth args 1 with LInt i2 -> LInt(i1 mod i2) | _ -> failwith "Bad args on Motu")
    | _ -> failwith "Bad args on Motu")
  | "Neke" -> (match List.nth args 0 with LInt i -> LInt(-i) | _ -> failwith "Bad args on Neke")
  | "Konka" -> LStr(List.fold_left
      (fun acc l -> acc ^ (string_of_value l) )
      "" args)
  | "Mute" -> LList(args)
  | "Konsu" ->
      let lit = List.nth args 0 in
      let lst = List.nth args 1 in
      (match lst with
      | LList l -> LList(lit::l)
      | _ -> failwith "Bad args on Konsu")
  | "Na" ->
      let sub = List.nth args 0 in
      let num = (match List.nth args 1 with LInt i -> i | _ -> failwith "Bad args on Na") in
      (match sub with
      | LStr s ->
        (try
          LStr(String.make 1 (s.[num]))
          with _ -> LStr(""))
      | LList l -> List.nth l num
      | _ -> failwith "Bad args on Na")
  | "Anpa" ->
      let sub = List.nth args 0 in
      (match sub with
      | LStr s -> LStr(String.sub s 1 ((String.length s) - 1))
      | LList l -> LList(List.tl l)
      | _ -> failwith "Bad args on Anpa")
  | "Insasin" ->
      let sub = List.nth args 0 in
      let num = (match List.nth args 1 with LInt i -> i | _ -> failwith "Bad args on Na") in
      let replace = List.nth args 2 in
      (match sub with
      | LList l -> LList(List.mapi (fun i a -> if i == num then replace else a) l)
      | _ -> failwith "Bad args on Insasin")
  | "Len" ->
      let sub = List.nth args 0 in
      (match sub with
      | LStr s -> LInt(String.length s)
      | LList l -> LInt(List.length l)
      | _ -> failwith "Bad args on Len")
  | "Asuki" ->
      let sub = List.nth args 0 in
      (match sub with
      | LInt i -> LStr(String.make 1 (Char.chr i))
      | _ -> failwith "Bad args on Asuki")
  | _ ->
      let num_args = List.length args in
      match fn_match (Hashtbl.find fn_table (name, num_args)) args with
      | None -> failwith "No matching function exists"
      | Some fn -> run_body fn.body (fn_table, globals) (bind_arg_ids fn.args args)

and run_expr ex env locs = match ex with
  | EVal v -> (match v with
    | Lit l -> l
    | Id (t, id) -> get_binding id env locs)
  | ECall (n, vl) -> fn_dispatch n (get_bindings vl env locs) env locs

and run_body b env (locs : bindings) = match b with
  | EBind (n, ex, b) ->
      let res = run_expr ex env locs in (Hashtbl.add locs n res ; run_body b env locs)
  | ERet ex -> run_expr ex env locs

(******************************************************************************)

let bind_globals_in_val v (fn_table, globals) locs = match v with
  | Id (t, id) -> if List.mem id locs
      then Id(t, id)
      else if Hashtbl.mem globals id
      then Lit(Hashtbl.find globals id)
      else failwith ("Unbound variable \"" ^ id ^ "\"!") (* shouldn't happen *)
  | Lit _ -> v

let rec bind_globals_in_vals vl env locs = match vl with
  | [] -> []
  | h::t -> (bind_globals_in_val h env locs)::(bind_globals_in_vals t env locs)

let bind_globals_in_expr expr env locs = match expr with
  | EVal v -> EVal(bind_globals_in_val v env locs)
  | ECall (n, vl) -> ECall(n, bind_globals_in_vals vl env locs)

(* Binds current globals within body to enforce immutability *)
(* Returns new body with all global references replaced with concrete values *)
let rec bind_globals body env locs = match body with
  | EBind (n, ex, bd) ->
      EBind(n, (bind_globals_in_expr ex env locs), (bind_globals bd env (n::locs)))
  | ERet ex -> ERet(bind_globals_in_expr ex env locs)

(******************************************************************************)

let eval_sentence s (fn_table, globals) =
  let env = (fn_table, globals) in
  let new_table = Hashtbl.create 3 in
  match s with
  | EFun (name, args, body) ->
      let num_args = List.length args in
      let new_args = bind_globals_in_vals args env (get_ids args) in
      let new_body = bind_globals body env (get_ids args) in
      let new_fn = [{name = name; args = new_args; body = new_body}] in
      (if Hashtbl.mem fn_table (name, num_args)
      then Hashtbl.add fn_table (name, num_args) ((Hashtbl.find fn_table (name, num_args)) @ new_fn)
      else Hashtbl.add fn_table (name, num_args) new_fn) ; env
  | EGlob (name, body) ->
      Hashtbl.add globals name (run_body body env new_table); env
  | EPrint body ->
      Printf.printf "%s" (string_of_value (run_body body env new_table)); env

let rec eval_prog (p: prog) (e: env) = match p with
  | [] -> ()
  | h::t -> let ev = eval_sentence h e in (flush_all () ; (eval_prog t ev))

let parse_file name =
  let chan = open_in name in
  let lexbuf = Lexing.from_channel chan in
  let p = Parser.main Lexer.token lexbuf in
    close_in chan;
    p

let main () =
  let p = parse_file Sys.argv.(1) in
  let fns = Hashtbl.create (List.length core_fns) in
  let _ = List.fold_left (fun acc n -> add_core_fn fns n ; ()) () core_fns in
  eval_prog p (fns, Hashtbl.create 5)
;;

main ()
