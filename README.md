# 1. Background

## Introduction
ilo is a functional programming language inspired by OCaml and Haskell. The lexicon and syntax is based on the constructed language Toki Pona. The goal was to see if I could make a programming language out of another language meant for speaking.

## Requirements
You must have OCaml installed along with OCamlyacc, OCamllex, OCamlbuild, and GNU Make. Most Linux distributions have these included with the standard OCaml installation by default, but some have them as separate packages (Red Hat and Fedora).

## Compilation and Running
To compile the intepreter, simply run `make`.

You can then run the interpreter executable using ilo program files as command-line arguments.

# 2. Using The Language

## Examples
Example programs reside in the included `lipu` directory.

## Hello World
To print something, simply type `ni li "Hello World"`, where the thing following "li" is some expression. Print statements can only happen at the top level and can't occur inside of functions. "ni li X" translates to "this is X" in English.

## Numeral System
Toki Pona has very few words for numbers.

- ala = 0
- wan = 1
- tu = 2
- luka = 5
- mute = 20

To express numbers you must stack multiple number words in descending order. The resulting number is the sum of all the numbers in the phrase.

- tu tu = 4
- tu wan = 3
- luka tu tu = 9

## Variables
Variables are declared with the `ijo` keyword followed by a name, followed by `li` and an expression. The name must begin with a capital letter and conform to Toki Pona's phonology. [Read this to understand Toki Pona's phonology.](http://tokipona.net/tp/janpije/okamasona2.php)

Example: `ijo Ku li tu tu`

This means "set the variable named Ku to 4". All variables in ilo are immutable.

## Functions
Function signatures are declared with the `nasin` keyword followed by a name, then a list of parameters, and finally the `la` keyword. Each function is a "flat" list of expressions, eventually ending with a `ni li X` phrase which has the function return X. The function below takes a number and return the result of adding 2 to it. The `pali Tasu Ku tu` phrase is calling the Tasu function (addition) on the variable Ku and the number 2.

    # f(x) = x + 2
    nasin Tasutu Ku la
      ni li pali Tasu Ku tu

Toki Pona's syntax does not feature embedded clauses and so the programming language's biggest restriction is that functions can't have multiple nested levels. This results in a somewhat cumbersome requirement to compose functions down to very small units of functionality. Functional style tends to favor small functions anyway, but this is a bit on the extreme side.

Additionally, the only looping construct is recursion. 

Below is the fibonacci function.
        
    nasin Piponasi ala li ala
    nasin Piponasi wan li wan
    nasin Piponasi Ku la
      ijo Kuwan li pali Siku Ku wan la
      ijo Kutu li pali Siku Ku tu la
      ijo Kuwan li pali Piponasi Kuwan la
      ijo Kutu li pali Piponasi Kutu la
      ni li pali Tasu Kuwan Kutu