type t = Any | Int | Str | Bool | List
type literal = LInt of int | LStr of string | LBool of bool | LList of literal list
type value = Id of t * string | Lit of literal

type expr =
  | EVal of value
  | ECall of string * (value list)

type body =
  | EBind of string * expr * body
  | ERet of expr

type sent =
  | EFun of string * value list * body
  | EGlob of string * body
  | EPrint of body

type prog = sent list

type fn = {
  name : string;
  args : value list;
  body : body; }
type bindings = (string, literal) Hashtbl.t
type fn_table = ((string * int), fn list) Hashtbl.t

type env = (fn_table * bindings)
