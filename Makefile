.PHONY = all,clean

all:
	rm -f *.native *.byte
	ocamlbuild -cflag -g -yaccflag -v -lflag -g interp.byte

clean:
	rm -f *.native *.byte
